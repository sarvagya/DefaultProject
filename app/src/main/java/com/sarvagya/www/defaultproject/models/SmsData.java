package com.sarvagya.www.defaultproject.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.sql.Date;

@Entity
public class SmsData {

    @PrimaryKey
    @NonNull
    @SerializedName("id")
    private int id;

    private String vendor_code;

    private String cfy;

    @SerializedName("sms_id")
    private int sms_id;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("message")
    private String message;

    private String response;

    private int sent_to_device;

    @SerializedName("sent")
    private int sent;

    private int hits;

    private String sent_via;

    private String last_tried;

    private String created_at;

    private String updated_at;

    private String deleted_at;

    public SmsData() {

    }

    public SmsData(@NonNull int id, String vendor_code, String cfy, int sms_id, String mobile, String message, String response, int sent_to_device, int sent, int hits, String sent_via, String last_tried, String created_at, String updated_at, String deleted_at) {
        this.id = id;
        this.vendor_code = vendor_code;
        this.cfy = cfy;
        this.sms_id = sms_id;
        this.mobile = mobile;
        this.message = message;
        this.response = response;
        this.sent_to_device = sent_to_device;
        this.sent = sent;
        this.hits = hits;
        this.sent_via = sent_via;
        this.last_tried = last_tried;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.deleted_at = deleted_at;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public String getVendor_code() {
        return vendor_code;
    }

    public void setVendor_code(String vendor_code) {
        this.vendor_code = vendor_code;
    }

    public String getCfy() {
        return cfy;
    }

    public void setCfy(String cfy) {
        this.cfy = cfy;
    }

    public int getSms_id() {
        return sms_id;
    }

    public void setSms_id(int sms_id) {
        this.sms_id = sms_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getSent_to_device() {
        return sent_to_device;
    }

    public void setSent_to_device(int sent_to_device) {
        this.sent_to_device = sent_to_device;
    }

    public int getSent() {
        return sent;
    }

    public void setSent(int sent) {
        this.sent = sent;
    }

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public String getSent_via() {
        return sent_via;
    }

    public void setSent_via(String sent_via) {
        this.sent_via = sent_via;
    }

    public String getLast_tried() {
        return last_tried;
    }

    public void setLast_tried(String last_tried) {
        this.last_tried = last_tried;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }
}
