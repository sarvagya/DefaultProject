package com.sarvagya.www.defaultproject.response;

import com.google.gson.annotations.SerializedName;

public class DeviceRegisterResponse {

    @SerializedName("status")
    private int status;

    @SerializedName("device_token")
    private String token;

    @SerializedName("sparrow_sms_token")
    private String sparrow_sms_token;

    @SerializedName("sparrow_sms_from")
    private String sparrow_sms_from;

    @SerializedName("message")
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSparrow_sms_token() {
        return sparrow_sms_token;
    }

    public void setSparrow_sms_token(String sparrow_sms_token) {
        this.sparrow_sms_token = sparrow_sms_token;
    }

    public String getSparrow_sms_from() {
        return sparrow_sms_from;
    }

    public void setSparrow_sms_from(String sparrow_sms_from) {
        this.sparrow_sms_from = sparrow_sms_from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
