package com.sarvagya.www.defaultproject.response;

import com.google.gson.annotations.SerializedName;

public class SparrowSmsResponse {

    @SerializedName("count")
    private int count;

    @SerializedName("response_code")
    private int response_code;

    @SerializedName("response")
    private String response;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
