package com.sarvagya.www.defaultproject.utils;

import android.util.Log;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class AppUtils {

    public static String getCurrentDate(){

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String currentDate = dateFormat.format(date);
        Log.d("sugam","currentDate"+currentDate);
        return currentDate;
    }
}
