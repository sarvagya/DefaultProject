package com.sarvagya.www.defaultproject.api;

import com.google.gson.JsonArray;
import com.sarvagya.www.defaultproject.models.SmsData;
import com.sarvagya.www.defaultproject.response.DeviceRegisterResponse;
import com.sarvagya.www.defaultproject.response.SmsDataResponse;
import com.sarvagya.www.defaultproject.response.SparrowSmsResponse;
import com.sarvagya.www.defaultproject.response.SparrowTokenResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ApiInterface {

    @POST
    @FormUrlEncoded
    Call<SmsDataResponse> getSmsData(@Url String url,
                                     @Header("imei") String imeiNumber,
                                     @Header("device-token") String token,
                                     @Field("sent-sms")String logStr);


    @POST
    @FormUrlEncoded
    Call<SparrowSmsResponse> sendSparrowSms(@Url String url,
                                            @Field("token") String token,
                                            @Field("from") String from,
                                            @Field("to") String number,
                                            @Field("text") String text);

    @GET
    Call<DeviceRegisterResponse> registerDevice(@Url String url,
                                                @Header("imei") String imeiNumber);

}
