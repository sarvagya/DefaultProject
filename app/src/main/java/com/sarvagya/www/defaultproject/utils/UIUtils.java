package com.sarvagya.www.defaultproject.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.sarvagya.www.defaultproject.R;

import java.util.Observable;

public class UIUtils {

    public static void closeSoftKeyboard(Activity activity, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            view.clearFocus();
        }
    }

    public static MaterialDialog runProgressDialog(Context context, String title, String body) {
        final MaterialDialog progressDialog = new MaterialDialog.Builder(context)
                .title(title)
                .content(body)
                .progress(true, 0)
                .show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        return progressDialog;
    }


}
