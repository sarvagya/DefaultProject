package com.sarvagya.www.defaultproject.utils;

import java.util.HashMap;

public class Constants {

    public static final HashMap<Integer, String> MONTH = new HashMap<Integer, String>() {{
        put(1, "January");
        put(2, "February");
        put(3, "March");
        put(4, "April");
        put(5, "May");
        put(6, "June");
        put(7, "July");
        put(8, "August");
        put(9, "September");
        put(10, "October");
        put(11, "November");
        put(12, "December");
    }};
}
