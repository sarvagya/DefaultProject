package com.sarvagya.www.defaultproject.response;

public class SparrowTokenResponse extends ResponseData{

    private String token;

    private String fromName;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }
}
