package com.sarvagya.www.defaultproject.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtil {

    private static Calendar calendar;

    public static String getMonthName(Integer month) {
        return Constants.MONTH.get(month);
    }

    public static String convert24to12(String time24Hour) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        try {
            Date date = parseFormat.parse(time24Hour);
            String time = displayFormat.format(date);
            int x = Integer.parseInt(time.substring(0, 1));
            if (x == 0) {
                time = time.substring(1);
            }
            return time;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convert12to24(String time12Hour) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        try {
            Date date = parseFormat.parse(time12Hour);
            return displayFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String getCurrent24HourTime(int additionalMinute) {
        calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:45"));
        if (additionalMinute > 0) {
            calendar.add(Calendar.MINUTE, additionalMinute);
        }
        Date currentTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        date.setTimeZone(TimeZone.getTimeZone("GMT+5:45"));
        /*-------------------------------------------------------*/
        String time = date.format(currentTime);

        String[] a = time.split(":");
        //hour = Integer.parseInt(a[0]);
        String[] b = a[1].split("\\s+");
        int minute = Integer.parseInt(b[0]);

        return time;
    }

    public static String getCurrent12HourTime(int additionalMinute) {
        calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:45"));
        if (additionalMinute > 0) {
            calendar.add(Calendar.MINUTE, additionalMinute);
        }
        Date currentTime = calendar.getTime();
        DateFormat date = new SimpleDateFormat("KK:mm a", Locale.ENGLISH);
        date.setTimeZone(TimeZone.getTimeZone("GMT+5:45"));
        /*-------------------------------------------------------*/
        String time = date.format(currentTime);

        String[] a = time.split(":");
        //hour = Integer.parseInt(a[0]);
        String[] b = a[1].split("\\s+");
        int minute = Integer.parseInt(b[0]);

        return time;
    }

    public static HashMap<String, Integer> getDate(int dayOffSet) {
        HashMap<String, Integer> dateMap = new HashMap<>();
        calendar = Calendar.getInstance();
        if (dayOffSet > 0) {
            calendar.add(Calendar.DAY_OF_MONTH, dayOffSet);
        }
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        dateMap.put("year", year);
        dateMap.put("month", month + 1);
        dateMap.put("day", day);

        return dateMap;
    }

    public static Integer getDateDifference(String lastDate) {
        HashMap<String, Integer> dateMap = getDate(0);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        if (lastDate.length() == 0) {
            lastDate = dateMap.get("year") + "-" + dateMap.get("month") + "-" + dateMap.get("day");
        }
        try {
            Date date1 = dateFormat.parse(dateMap.get("year") + "-" + dateMap.get("month") + "-" + dateMap.get("day"));
            Date date2 = dateFormat.parse(lastDate);

            Long difference = Math.abs(date1.getTime() - date2.getTime()) / (24 * 60 * 60 * 1000);

            return Integer.parseInt(difference.toString());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }


}
