package com.sarvagya.www.defaultproject.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MessageData {

    @SerializedName("current_page")
    private int current_page;

    @SerializedName("data")
    private ArrayList<SmsData> smsDataArrayList;

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public ArrayList<SmsData> getSmsDataArrayList() {
        return smsDataArrayList;
    }

    public void setSmsDataArrayList(ArrayList<SmsData> smsDataArrayList) {
        this.smsDataArrayList = smsDataArrayList;
    }

    /*         "first_page_url": "https://staging.pokharaskysports.com/api/sms/receive?page=1",
                "from": 1,
                "last_page": 1,
                "last_page_url": "https://staging.pokharaskysports.com/api/sms/receive?page=1",
                "next_page_url": null,
                "path": "https://staging.pokharaskysports.com/api/sms/receive",
                "per_page": 10,
                "prev_page_url": null,
                "to": 1,
                "total": 1*/
}
