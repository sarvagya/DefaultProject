package com.sarvagya.www.defaultproject.injection.components;

import android.app.Activity;
import android.content.Context;

import com.sarvagya.www.defaultproject.HistoryActivity;
import com.sarvagya.www.defaultproject.activities.HistoryAdapter;
import com.sarvagya.www.defaultproject.activities.MainActivity;
import com.sarvagya.www.defaultproject.activities.RegisterActivity;
import com.sarvagya.www.defaultproject.service.DataDownloaderService;
import com.sarvagya.www.defaultproject.injection.modules.ApiInterfaceModule;
import com.sarvagya.www.defaultproject.injection.modules.PicassoModule;
import com.sarvagya.www.defaultproject.injection.scopes.PerActivityScope;

import dagger.Component;

@PerActivityScope
@Component(modules = {ApiInterfaceModule.class, PicassoModule.class})
public interface MyAppComponent {

    void injectMainActivity(MainActivity mainActivity);

    void injectDataDownloaderService(DataDownloaderService dataDownloaderService);

    void injectRegisterActivity(RegisterActivity registerActivity);

    void injectHistoryActivity(HistoryActivity historyActivity);




}
