package com.sarvagya.www.defaultproject.utils;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.util.Log;

import com.sarvagya.www.defaultproject.database.Mydatabase;
import com.sarvagya.www.defaultproject.models.SmsData;

import java.util.ArrayList;
import java.util.List;

public class LogGenerator {
    private static String log;

    private static Mydatabase db;
    private static final String DATABASE_NAME = "Sms_Data";


    public static List<SmsData> getLog(Context context) {

        db = Room.databaseBuilder(context, Mydatabase.class, DATABASE_NAME)
                .allowMainThreadQueries().build();


        List<SmsData> smsList = db.smsDataDao().getSentSms(10);


            return smsList;
    }
}
