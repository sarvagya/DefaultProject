package com.sarvagya.www.defaultproject.response;

import com.google.gson.annotations.SerializedName;
import com.sarvagya.www.defaultproject.models.MessageData;
import com.sarvagya.www.defaultproject.models.SmsData;

import java.util.ArrayList;

public class SmsDataResponse {

    @SerializedName("status")
    private int status;

    @SerializedName("data")
    private MessageData data;

    @SerializedName("update_ids")
    private String update_ids;

    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public MessageData getData() {
        return data;
    }

    public void setData(MessageData data) {
        this.data = data;
    }

    public String getUpdate_ids() {
        return update_ids;
    }

    public void setUpdate_ids(String update_ids) {
        this.update_ids = update_ids;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
