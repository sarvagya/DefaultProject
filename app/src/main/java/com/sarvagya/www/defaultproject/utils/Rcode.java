package com.sarvagya.www.defaultproject.utils;

public class Rcode {
    public static final int SUCCESS = 200;
    public static final int ERROR = 400;
    public static final int TOKEN_EXPIRED = 440;
    public static final int SERVER_ERROR = 500;
}
